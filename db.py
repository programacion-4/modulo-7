
from sqlalchemy.orm import sessionmaker

from sqlalchemy_utils import database_exists, create_database

from sqlalchemy_serializer import SerializerMixin

from sqlalchemy import create_engine,MetaData,Column, String,Table

from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base() 
class Modelo (Base,SerializerMixin):
    __tablename__= "Palabras"
    palabra = Column(String,primary_key=True)
    definicion = Column(String)
    def __init__(self,palabra,definicion):
        self.palabra = palabra
        self.definicion = definicion
def createTable(engine): 
    meta = MetaData()
    mydb = Table(
    'Palabras',meta,
    Column('palabra',String(1000),primary_key=True),
    Column('definicion',String(1000)))
    meta.create_all(engine)
class ORM():
    def __init__(self,nameTable,dialect): 
        self.nameTable = nameTable+'.db' 
        self.dialect = dialect+self.nameTable +'?check_same_thread=False' 
        self.engine = create_engine(self.dialect)  
        if not database_exists(self.engine.url): 
            create_database(self.engine.url)
            createTable(self.engine)
        self.session = sessionmaker(bind = self.engine)() 
    def agregarPalabra(self,palabra,definicion):
        self.session.add(Modelo(palabra,definicion))
        self.session.commit()
    def obtenerPalabra(self,palabra):
        try:
            resultado = self.session.query(Modelo).filter(Modelo.palabra == palabra).one()
            return resultado
        except:
            pass
    def obtenerTodo(self):
        resultado = self.session.query(Modelo).all()
        return resultado
    def editarPalbra(self,palabra,newPalabra):
        resultado = self.session.query(Modelo).filter(Modelo.palabra == palabra).one()
        resultado.palabra = newPalabra
        self.session.add(resultado)
        self.session.commit()
    def editarDefinicion(self,palabra,definicion):
        resultado = self.session.query(Modelo).filter(Modelo.palabra == palabra).one()
        resultado.definicion = definicion
        self.session.add(resultado)
        self.session.commit()
    def borrarPalabra(self,palabra):
        resultado = self.session.query(Modelo).filter(Modelo.palabra == palabra).one()
        self.session.delete(resultado)
        self.session.commit()
    
