from flask_restful import Api, Resource, reqparse
from flask import Flask,jsonify,render_template,request
from db import ORM
from marshmallow import Schema, fields


class Model(Schema):
    palabra = fields.Str()
    definicion = fields.Str()

app = Flask(__name__)
api = Api(app)

Palabras = [{'id':1,'title':'mtitle'}]

class restApi(Resource):
     dialectoSQLITE = 'sqlite:///'
     orm = ORM('Palabras',dialectoSQLITE)
     def get(self, palabra=''): 
         if palabra == '':
            return Model(many=True).dump(self.orm.obtenerTodo()), 200
         
         if Model().dump(self.orm.obtenerPalabra(palabra)):
             return Model().dump(self.orm.obtenerPalabra(palabra))
         else:
             return "No hay resultado", 404

     def post(self, palabra):
        parser = reqparse.RequestParser()
        parser.add_argument("definicion")
        params = parser.parse_args()
        try:
            self.orm.agregarPalabra(palabra,params['definicion'])
            return palabra, 201
        except:
            return f"{palabra} ya existe", 400
        

     def put(self, palabra):
        parser = reqparse.RequestParser()
        parser.add_argument("definicion")
        parser.add_argument("nuevaPalabra")
        params = parser.parse_args()
        try:
            self.orm.editarDefinicion(palabra,params['definicion'])
            self.orm.editarPalbra(palabra,params['nuevaPalabra'])
            return 'exito', 200
        except:
            return f"{palabra} no  existe", 400

     def delete(self, palabra):
      try:
         self.orm.borrarPalabra(palabra)
         return f"{palabra} ha sido eliminado", 200 
      except:
          return f"{palabra} no existe", 400
      

api.add_resource(restApi, "/palabra", "/palabra/", "/palabra/<string:palabra>")

@app.route('/')
def index():
    dialectoSQLITE = 'sqlite:///'
    orm = ORM('Palabras',dialectoSQLITE) 
    tabla = orm.obtenerTodo()
    return render_template('index.html',tabla=tabla)

if __name__ == '__main__':
    app.run(debug=True)
